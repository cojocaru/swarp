#/*##########################################################################
#
# SWaRP: Speckle Wavefront Reconstruction Package 
#
# Copyright (c) 2016-2018 European Synchrotron Radiation Facility
#
# This file is part of the SWaRP Speckle Wavefront Reconstruction Package
# developed at the ESRF by the staff of BM05 as part of EUCALL WP7:PUUCA.
#
# This project has received funding from the European Union’s Horizon 2020 
# research and innovation programme under grant agreement No 654220.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
#############################################################################*/

__author__ = ['Ruxandra Cojocaru', 'Sebastien Berujon']
__contact__ = 'cojocaru@esrf.fr; sebastien.berujon@esrf.fr'
__license__ = 'MIT'
__copyright__ = 'European Synchrotron Radiation Facility, Grenoble, France'
__date__ = '08/11/2018'

import swarp
